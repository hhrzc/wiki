**Java**

- **Solutions**
[Generic implement to implement example](https://gitlab.com/hhlTer/wiki/-/wikis/generic-complex-using)

- **J**<br/>
[Create and use jar library](https://gitlab.com/hhlTer/wiki/-/wikis/Gradle-create-jar-library)
[JPA - create schema from sql file](https://gitlab.com/hhlTer/wiki/-/wikis/sql-file-initialization)

- **M**<br/>
[Mail sending.](https://gitlab.com/hhlTer/wiki/-/wikis/Gmail.-Send-mail)

- **P**<br/>
[Plugin for IntelliJIdea](https://gitlab.com/hhrzc/wiki/-/wikis/IntelliJ-plugin)

- **R**<br/>
[RestAssured. `application/x-www-form-urlencoded` using](https://gitlab.com/hhlTer/wiki/-/wikis/cannot-determine-how-to-serialize-content-type-application/Content-Type=application-x-www-form-urlencoded-issue-fixing)<br/>
[Regex](https://gitlab.com/hhlTer/wiki/-/wikis/Regex)

- **S**<br/>
[Web security](https://gitlab.com/hhlTer/wiki/-/wikis/home)<br/>
[Security implementation between frontend and API]()<br/>

- **Selenium**<br/>
[Clear cookies with selenium](https://gitlab.com/hhlTer/wiki/-/wikis/Clear-cookies-with-selenium-method)<br/>
[Firefox profile using!](https://stackoverflow.com/questions/44389653/using-selenium-on-https-web-application-that-runs-over-vpn)<br/>

- **Swagger**<br/>
[Setup Swagger](https://gitlab.com/hhlTer/wiki/-/wikis/Swagger)<br/>

- **T**<br/>
[Tuple](https://gitlab.com/hhrzc/wiki/-/wikis/Tuple-function)<br/>

- **Testing**
[TestNG_groups](https://gitlab.com/hhrzc/wiki/-/wikis/TestNG-groups)<br/>
[TestNG_plus_Bean_Configuration](https://gitlab.com/hhrzc/wiki/-/wikis/TestNG-bean-conf)<br/>

**React**<br/>
[npm nodejs install](https://gitlab.com/hhrzc/wiki/-/wikis/npm-node-install)<br/>


**DevOps**

- **AWS**<br/>
[AWS NOTES. EC2](https://gitlab.com/hhlTer/wiki/-/wikis/EC2-Notes)<br/>
[AWS NOTES. ELB](https://gitlab.com/hhlTer/wiki/-/wikis/EBS)<br/>
[AWS EC2: image cration](https://gitlab.com/hhlTer/wiki/-/wikis/Create-instance)<br/>
[AWS EC2: instance creation](https://gitlab.com/hhlTer/wiki/-/wikis/AWS_instance_creation), [NEW console](https://gitlab.com/hhlTer/wiki/-/wikis/New-Console-EC2-instance-creation)<br/>
[AWS EC2: EBS Volume Encrypting](https://gitlab.com/hhlTer/wiki/-/wikis/EBS-Encription)<br/>
[AWS EC2: Snapshots](https://gitlab.com/hhlTer/wiki/-/wikis/Snapshots.-Volume)<br/>
[AWS EC2: EBS volume](https://gitlab.com/hhlTer/wiki/-/wikis/EBS-Volume-creation)<br/>
[AWS EC2: NI: Attaching NI to the instance](https://gitlab.com/hhlTer/wiki/-/wikis/Elastic-Network-Interfaces.-Launch-instances)<br/>
[AWS EC2: Hibernating](https://gitlab.com/hhlTer/wiki/-/wikis/Hibernate)<br/>
[AWS EC2: Placement group](https://gitlab.com/hhlTer/wiki/-/wikis/Placement-groups)<br/>
[AWS EC2: user notes: httpd installing example](https://gitlab.com/hhlTer/wiki/-/wikis/httpd-example)<br/>
[AWS EC2: connect to the instance](https://gitlab.com/hhlTer/wiki/-/wikis/Connect-to-the-instance-by-RDC)<br/>
[AWS EC2: Web Server starting](https://gitlab.com/hhlTer/wiki/-/wikis/Web-Server-starting)<br/>
[AWS Peering connection](https://gitlab.com/hhlTer/wiki/-/wikis/VPC-peering)<br/>
[AWS ASG: ASG creation](https://gitlab.com/hhlTer/wiki/-/wikis/ASG.2)<br/>
[AWS RDS: creation](https://gitlab.com/hhlTer/wiki/-/wikis/RDS)<br/>, [Second tutorial](https://gitlab.com/hhlTer/wiki/-/wikis/RDS-creation.-2)
[AWS EFS: Create EFS for two EC2](https://gitlab.com/hhlTer/wiki/-/wikis/EFS)<br/>
[AWS IAM: create users](https://gitlab.com/hhlTer/wiki/-/wikis/Create-users)<br/>
[AWS IAM: roles](https://gitlab.com/hhlTer/wiki/-/wikis/roles)<br/>
[AWS IAM: MFA setting](https://gitlab.com/hhlTer/wiki/-/wikis/MFA-creation)<br/>
[AWS IAM: CLI - user registration / aws configure](https://gitlab.com/hhlTer/wiki/-/wikis/CLI.-Users-registration)<br/>
[AWS IAM: sequrity key creation](https://gitlab.com/hhlTer/wiki/-/wikis/AWS-sequrity-key-creation)<br/>
[AWS ELB: CLB, ALB, NLB, GWLB](https://gitlab.com/hhlTer/wiki/-/wikis/Classic-Load-Balancer)<br/>
[AWS ELB: Sticky Sessions](https://gitlab.com/hhlTer/wiki/-/wikis/Sticky-Sessions)<br/>
[AWS ELB: Certificates](https://gitlab.com/hhlTer/wiki/-/wikis/Certificates-managing)<br/>
[AWS S3: copy files](https://gitlab.com/hhlTer/wiki/-/wikis/Copy-files-to-S3-bucket-with-crontab)<br/>
[AWS S3: create bucket with CLI](https://gitlab.com/hhlTer/wiki/-/wikis/Create-S3-with-CLI)<br/>
[AWS S3: static website](https://gitlab.com/hhlTer/wiki/-/wikis/Static-websites)<br/>
[AWS S3: folder synchronization](https://gitlab.com/hhlTer/wiki/-/wikis/S3-bucket-synchronization)<br/>
[AWS DynamoDB creation](https://gitlab.com/hhlTer/wiki/-/wikis/DynamoDB)<br/>
[AWS common information](https://gitlab.com/hhlTer/wiki/-/wikis/Common-information)<br/>
[AWS Jmeter best practics](https://gitlab.com/hhlTer/wiki/-/wikis/AWS-Jmeter-best-practics)<br/>
[AWS API Gateway](https://gitlab.com/hhlTer/wiki/-/wikis/AWS-API-Gateway)<br/>
[AWS API Gateway - DynamoDB integration](https://gitlab.com/hhlTer/wiki/-/wikis/AWS-API-DynamoDB)<br/>

- **AWS** upper<br/>
[AWS create template configuration (scaling settings)](https://gitlab.com/hhlTer/wiki/-/wikis/Auto-Scaling-Configuration)<br/>
[AWS Auto-Scaling group](https://gitlab.com/hhlTer/wiki/-/wikis/Auto-Scaling-group)<br/>
[AWS Teplates](https://gitlab.com/hhlTer/wiki/-/wikis/Create-the-Launch-template)<br/>

- **Instalation**  
[Centos](https://gitlab.com/hhlTer/wiki/-/wikis/NodeJs_npm_installing)<br/>
[ngnix](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-20-04)<br/>
[Plugins](https://gitlab.com/hhrzc/wiki/-/wikis/CheckMateX-plugins-installation)

- **Gradle**<br/>
[Groovy introduction](https://gitlab.com/hhlTer/wiki/-/wikis/Groovy)<br/>
[Task ordering](https://gitlab.com/hhlTer/wiki/-/wikis/StepsOrdering)<br/>
[Project name changing](https://gitlab.com/hhlTer/wiki/-/wikis/Change-build-name)


- **A**<br/>
[Apache server. Installing](https://gitlab.com/hhlTer/wiki/-/wikis/Install-apache-server)


- **B**<br/>
[Bat](https://gitlab.com/hhlTer/wiki/-/wikis/Bash.-Get-file-from-explorer)

- **C**<br/>
[Copy file to the server from windows](https://gitlab.com/hhlTer/wiki/-/wikis/Copy-file-from-Windows-to-the-Linux)<br/>

- **D**<br/>
[Docker](https://gitlab.com/hhlTer/wiki/-/wikis/Docker-education)<br/>
[Spring+MySQL](https://stackoverflow.com/questions/75344232/run-java-application-spring-boot-mysql-by-docker-compose)

- **F**<br/>
[Facebook API](https://www.youtube.com/watch?v=to4uTxSNo6Q)

- **G**<br/>
[GIT create dependency](https://levelup.gitconnected.com/publish-your-first-package-with-gradle-and-gitlab-1993e4405629)<br/>
[GIT copy files to S3](https://gitlab.com/hhlTer/wiki/-/wikis/Copy-files-to-S3-bucket-from-GIT-repo)<br/>
[GIT log, GIT show](https://levelup.gitconnected.com/use-git-like-a-senior-engineer-ef6d741c898e)<br/>
[GIT node.js deploying](https://gitlab.com/hhlTer/wiki/-/wikis/Deploy-nodejs-project)<br/>
[GIT install gitlab](https://gitlab.com/hhlTer/wiki/-/wikis/gitlab-install)<br/>

- **J**<br/>
[Jenkins recovery with docker](https://gitlab.com/hhlTer/wiki/-/wikis/Jenkins-recovery-with-docker)<br/>
[Jenkins. Connect to GIT with SSH](https://gitlab.com/hhlTer/wiki/-/wikis/Jenkins.-Connect-to-GIT-with-SSH)<br/>
[Jenkins. Full establishing](https://gitlab.com/hhlTer/wiki/-/wikis/Jenkins-full-establishing)<br/>

- **P**<br/>
[Pangolin installation]()

- **R** <br/>
[RedHat yum update roll back](https://gitlab.com/hhlTer/wiki/-/wikis/Yum-update-issue-fixed/Yum-update-issue-fixed-(roll-back))<br/>

- **S**<br/>
[Selenoid guid](https://gitlab.com/hhlTer/wiki/-/wikis/Selenoid-guid)<br/>
[Selenium grid installation](https://gitlab.com/hhlTer/wiki/-/wikis/Selenium-grid-installation)<br/>
[SSH connection](https://gitlab.com/hhlTer/wiki/-/wikis/SSH-connection-(in-progress))

- **Smart links**<br/>
[TestNG - Retry failed tests](https://www.swtestacademy.com/retry-failed-tests-testng-iretryanalyzer/)<br/>
[TestNG - ](https://testng.org/doc/documentation-main.html#factories)<br/>

- **Certificates**
[Splunk](https://gitlab.com/hhlTer/wiki/-/wikis/Certs)

